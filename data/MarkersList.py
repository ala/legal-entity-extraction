class MarkersList:
    def getLocationMarkers(self):
        return ["voirie", "voie", "territoire", "atelier", "luxembourg", "fourrière", "centre", "emplacement", "route",
                "itinéraire", "voies", "emplacements", "itinéraires", "chemins", "centres", "agglomérations",
                "endroits",
                "tronçons", "places"]

    def getConditionMarkers(self):
        return ["dans", "cas", "limite", "limites", "formes", "conditions", "hypothèse", "en ", "par", "en cas d'",
                "en cas de", "lorsqu'", "lorsque", "au moins", "dans le cadre des", "si", "dès que", "en cas de"]

    def getExceptionMarkers(self):
        return ["exception", "exceptions", "sans", "sauf", "moins", "dérogation"]

    def getActorMarkers(self):
        return ["organismes", "ministre", "inspecteur", "contrevenant", "membre", "organisme", "intéressé", "armée",
                "chef", "collège", "échevins", "conseil", "personne", "membres", "services", "service", "protection",
                "résidents", "médecin", "procureur", "propriétaire", "détenteur", "experts", "personnel", "conducteur",
                "système", "riverains", "usagers", "communes", "chauffeurs", "juge", "réviseur", "structures",
                "parlement", "commission", "gouvernement", "fonctionnaires", "mandataire", "entrepreneur", "police",
                "dépanneurs", "personnnes", "représentants", "représentant", "maître", "titulaire", "union",
                "opérateur", "installateur"]

    def getModalityMarkers(self):
        return ["doivent", "doit", "soumis", "interdit", "applique", "peut", "requis", "tenu", "puisse"]

    def getTimeMarkers(self):
        return ["avant", "avant d'", "plus tard", "délai", "mois", "jours", "à compter du", "ans", "juin", "septembre",
                "permanent", "moment", "immédiatement", "durée", "délais", "pendant", "provisoire", "temporaire",
                "à partir de", "jour", "durées", "dès qu'", "lors du", "avant de", "date", "période", "à compter de",
                "dès", "juillet", "heures", "an", "février", "mars", "avril", "constamment"]

    def getReasonMarkers(self):
        return ["visé", "visée", "visées", "prévues", "prévue", "servant", "afin de", "en vue de", "en vue d'",
                "à effet"]

    def getArtifactMarkers(self):
        return ["équipements", "embrayage", "taxe", "taxes", "avertissement", "délai", "delais", "autorisations",
                "cours"]

    def getReferenceMarkers(self):
        return ["disposition", "loi", "règlement", "règlements", "arrêté", "article", "paragraphe", "dispositions",
                "alinéa", "mémorial"]

    def getViolationMarkers(self):
        return ["infractions", "contraventions", "délit", "infraction", "délits", "contravention"]

    def getSituationMarkers(self):
        return ["exécution", "réduction", "délivrance", "contrôle", "publication", "usage", "accord", "paiement",
                "action", "demande", "refus", "autorisations", "équipement", "protection", "conception", "affectation",
                "appréciation", "stationnement", "ivresse", "examen", "prise", "emprise", "validité", "transcription",
                "instruction", "qualification", "emprisonnement", "opérations", "route", "circulation", "limitations",
                "élimination", "mise", "report", "analyse", "détermination", "dédommagement", "attribution",
                "réception", "envoi", "introduction", "exercice", "honorabilité", "direction", "gestion",
                "reconstitution", "option", "surveillance", "ouverture", "composition", "constatation", "décision",
                "indication", "participation", "procédure", "transports", "aménagement", "transport", "dépassement",
                "vérification", "détention", "accès", "tension", "inscription", "cours", "contrôles", "diligence",
                "liquidation", "identification", "location", "attributions", "service", "distribution", "pression",
                "façon", "mesures", "illustrations", "dégradation", "sénescence", "adaptation"]

    def getSanctionMarkers(self):
        return ["avertissement", "condamnation", "amende", "peine", "restriction", "suspension", "taxes", "retrait",
                "emprisonnement", "peines", "avertissements", "interdiction", "peines"]
