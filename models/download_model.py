from huggingface_hub import snapshot_download

#snapshot_download(repo_id="mistralai/Mixtral-8x7B-Instruct-v0.1", cache_dir="../temp", local_dir="./Mixtral-8x7B-Instruct-v0.1")
#snapshot_download(repo_id="mistralai/Mistral-7B-Instruct-v0.2", cache_dir="../temp", local_dir="./Mistral-7B-Instruct-v0.2")
#snapshot_download(repo_id="152334H/miqu-1-70b-sf", cache_dir="../temp", local_dir="./Miqu-1-70b")
#snapshot_download(repo_id="camembert/camembert-base", cache_dir="../temp", local_dir="./CamemBERT-base")
snapshot_download(repo_id="maastrichtlawtech/legal-camembert-base", cache_dir="../temp", local_dir="./LegalCamemBERT-base")