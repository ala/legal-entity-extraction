import json
import numpy as np
import plotly.graph_objects as go


def build_grouped_barchart(config, title, output_path):
    bars = []

    for i in range(len(config)):
        bars.append([])

    for i in range(len(config)):
        sum_perfect_equals = 0
        sum_subpart = 0
        sum_not_covered = 0
        sum_out_of_scope = 0

        with open(config[i]['ref']) as file:
            data = json.load(file)
            data = data["break_down"]
            for tag, values in data.items():
                sum_perfect_equals += values['perfect_equals']
                sum_subpart += values['subpart']
                sum_not_covered += values['not_covered']
                sum_out_of_scope += values['out_of_scope']

        bars[i].append(sum_perfect_equals)
        bars[i].append(sum_subpart)
        bars[i].append(sum_not_covered)
        bars[i].append(sum_out_of_scope)

    print(bars)


config = [
    {
        'ref': '../../results/LLM/GPT-4/GPT-4_few_shot_results.json',
        'title': 'GPT-4 (one-shot)',
        'color': '#436850'
    },
    {
        'ref': '../../results/LLM/Miqu-1-70b/MIQU_zero_shot_results.json',
        'title': 'Miqu-1-70b (zero-shot)',
        'color': '#B784B7'
    },
    {
        'ref': '../../results/LLM/Mixtral-8x7b/MIXTRAL_zero_shot_results.json',
        'title': 'Mixtral-8x7b (zero-shot)',
        'color': '#e46400'
    },
    {
        'ref': '../../results/LLM/Mistral-7b/MISTRAL_fine_tuned_results.json',
        'title': 'Mistral-7b (fine-tuned)',
        'color': '#3ba8ca'
    }
]

title = "Evolution from Zero-shot to Fine-tuning predicate extraction with Mixtral"
output_path = "../../results/LLM/fig_breakdown_best_models.png"

build_grouped_barchart(config, title, output_path)