import json
import importlib.util
import pandas as pd

spec = importlib.util.spec_from_file_location("recall_precision_f1_f2", "./recall_precision_f1_f2.py")
recall_precision_f1_f2 = importlib.util.module_from_spec(spec)
spec.loader.exec_module(recall_precision_f1_f2)


#path = "../../results/LLM/GPT-4/GPT-4_zero_shot_"

path = "../../results/LLM/GPT-4/GPT-4_zero_shot_v2_"

#path="../../results/LLM/Mixtral-8x7b/MIXTRAL_zero_shot_"

#path="../../results/LLM/Mistral-7b/MISTRAL_zero_shot_"

#path="../../results/LLM/Miqu-1-70b/MIQU_zero_shot_"

#path = "../../results/LLM/GPT-4/GPT-4_few_shot_"

#path="../../results/LLM/Mixtral-8x7b/MIXTRAL_few_shot_"

#path="../../results/LLM/Mistral-7b/MISTRAL_few_shot_"

#path="../../results/LLM/Miqu-1-70b/MIQU_few_shot_"

#path="../../results/LLM/Mistral-7b/MISTRAL_fine_tuned_"

#path="../../results/LLM/Mixtral-8x7b/MIXTRAL_fine_tuned_"

#path="../../results/LLM/Miqu-1-70b/MIQU_fine_tuned_"


with open('../../data/evalQS.json', 'r') as fichier:
    eval_data_temp = json.load(fichier)

eval_data = {}
for sentence, classes in eval_data_temp.items():
    eval_data[sentence] = {}
    for tag, values in classes.items():
        eval_data[sentence][tag] = []
        for value in values:
            eval_data[sentence][tag].append(value.replace(" ", "").lower())

with open(path+"cleaned.json", 'r') as fichier:
    llm_data_temp = json.load(fichier)

llm_data = {}
for sentence, classes in llm_data_temp.items():
    llm_data[sentence] = {}
    for tag, values in classes.items():
        llm_data[sentence][tag] = []
        for value in values:
            llm_data[sentence][tag].append(value.replace(" ", "").lower())

del eval_data_temp, llm_data_temp

output = {'global': {}, 'break_down': {}}

#####################################################
#
#               Break down results
#
#####################################################

output['break_down'] = {element: {
    'perfect_equals': 0,
    'subpart': 0,
    'not_covered': 0,
    'out_of_scope': 0
} for element in
    ['action', 'actor', 'artifact', 'condition', 'location', 'modality', 'reference', 'time']}

columns = ['status', 'concept', 'eval_annot', 'llm_annot', 'sentence']
df = pd.DataFrame(columns=columns)

for sentence, classes in eval_data.items():
    for tag, values in classes.items():

        actual_perfect_equals = 0
        actual_subpart = 0

        if tag not in output['break_down']:
            continue

        if sentence not in llm_data:
            continue

        if tag in llm_data[sentence]:
            llm_values = llm_data[sentence][tag]
        else:
            llm_values = []

        # if annotation is fully equal
        for value in values:
            if value in llm_values:
                actual_perfect_equals += 1
                output['break_down'][tag]['perfect_equals'] += 1
                df = df._append({
                    'status': 'perfect_equals',
                    'eval_annot': value,
                    'llm_annot': value,
                    'sentence': sentence,
                    'concept': tag
                }, ignore_index=True)
                llm_values.remove(value)
                continue

        # sub-part in same class
        for value in values:
            for eval_sentence in llm_values:
                if value in eval_sentence or eval_sentence in value:
                    actual_subpart += 1
                    output['break_down'][tag]['subpart'] += 1
                    df = df._append({
                        'status': 'subpart',
                        'eval_annot': eval_sentence,
                        'llm_annot': value,
                        'sentence': sentence,
                        'concept': tag
                    }, ignore_index=True)
                    llm_values.remove(eval_sentence)
                    break

        # Out of scope
        output['break_down'][tag]['out_of_scope'] += len(llm_values)
        for value in llm_values:
            df = df._append({
                'status': 'out_of_scope',
                'eval_annot': '',
                'llm_annot': value,
                'sentence': sentence,
                'concept': tag
            }, ignore_index=True)

        # Annotation not covered
        output['break_down'][tag]['not_covered'] += (len(values) - (actual_perfect_equals + actual_subpart))

#####################################################
#
#    Recall Precision F1 F2 results
#
#####################################################

fn = 0
tp = 0
fp = 0

for tag, values in output['break_down'].items():

    fn += values['not_covered']
    tp += values['perfect_equals'] + values['subpart']
    fp += values['out_of_scope']

print(fn, tp, fp)
output['global'] = recall_precision_f1_f2.recall_precision_f1_f2(fn, tp , fp)

with open(path+"results.json", 'w', encoding='utf-8') as file:
    json.dump(output, file)

df.to_csv(path+'full_results.csv', index=False)