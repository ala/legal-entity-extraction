def recall_precision_f1_f2(fn, tp , fp):


    if tp + fp != 0:
        precision = tp / (tp + fp)
    else:
        precision = 0

    if tp + fn != 0:
        recall = tp / (fn + tp)
    else:
        recall = 0

    if precision != 0 and recall != 0:
        f1 = (2 * precision * recall) / (precision + recall)
        f2 = (5 * precision * recall) / (4 * precision + recall)
    else:
        f1 = 0
        f2 = 0

    return {'recall': recall, 'precision': precision, 'f1': f1, 'f2': f2}

#print(recall_precision_f1_f2(2, 91, 2))