import json
import plotly.graph_objects as go
import pandas as pd

results = {
    'concepts' : ['action', 'actor', 'artifact', 'condition', 'location', 'modality', 'reference', 'time'],
    'Evaluation' : [0, 0, 0, 0, 0, 0, 0, 0],
    'Training' : [0, 0, 0, 0, 0, 0, 0, 0]
}

with open('../../data/evalQS.json', 'r') as fichier:
    eval_data = json.load(fichier)

for question, tags in eval_data.items():
    for tag, annot in tags.items():
        if tag not in ['action', 'actor', 'artifact', 'condition', 'location', 'modality', 'reference', 'time']:
            continue

        index = results['concepts'].index(tag)
        results['Evaluation'][index] += len(annot)

with open('../../data/dataQS.json', 'r') as fichier:
    train_data = json.load(fichier)

for question, tags in train_data.items():
    for tag, annot in tags.items():
        if tag not in ['action', 'actor', 'artifact', 'condition', 'location', 'modality', 'reference', 'time']:
            continue

        index = results['concepts'].index(tag)
        results['Training'][index] += len(annot)


df = pd.DataFrame(results)

fig = go.Figure()

for col in df.columns[1:]:
    fig.add_trace(go.Bar(
            x=df['concepts'],
            y=df[col],
            name=col,
            text=df[col],
            textposition='auto'
        )
    )

fig.update_layout(xaxis_title='Concepts',
                  yaxis_title='Count')

fig.write_image("../../results/LLM/fig_global_concepts_distribution.png", format='png', engine='kaleido', scale=4)