import importlib
import json, Levenshtein
import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import pandas as pd

spec = importlib.util.spec_from_file_location("recall_precision_f1_f2", "./recall_precision_f1_f2.py")
recall_precision_f1_f2 = importlib.util.module_from_spec(spec)
spec.loader.exec_module(recall_precision_f1_f2)


def build_levenshtein_boxplot(config, title, output_path):
    x_axe = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    lines = []

    for i in range(len(config)):

        current_line = {element: 0 for element in x_axe}
        df = pd.read_csv(config[i]['data_path'])

        perfect_match = 0


        for index, row in df.iterrows():
            if row['status'] == 'perfect_equals':
                perfect_match += 1
                continue

            if row['status'] == 'subpart':
                try:
                    score = Levenshtein.distance(row['eval_annot'], row['llm_annot'])/max(len(row['eval_annot']), len(row['llm_annot']))
                except:
                    None

                for key, value in current_line.items():
                    if key - 0.1 < score < key:
                        current_line[key] += 1
                        break

        #print(perfect_match, current_line)

        result = []
        sum = 0
        for key, value in current_line.items():
            sum += value
            result.append(recall_precision_f1_f2.recall_precision_f1_f2(config[i]['false_negative'], perfect_match + sum , config[i]['false_positive'])['f1'])

        lines.append(result)

    #print(lines)
    fig = go.Figure()
    for i in range(len(config)):
        fig.add_trace(go.Scatter(x=x_axe, y=lines[i], mode='lines+markers', name=config[i]['title']))

    fig.update_layout(
                      xaxis_title='Normalized Levenshtein Distance Threshold',
                      yaxis_title='F1 Score')

    fig.write_image(output_path, format='png', engine='kaleido', scale=4)



config = [
    {
        'data_path': '../../results/LLM/GPT-4/GPT-4_few_shot_full_results.csv',
        'title': 'GPT-4 (one-shot)',
        'color': '#436850',
        'false_negative': 327,
        'false_positive': 288
    },

]

title = "Levenshtein Distance of GPT-4_ZS, Mixtral_ZS & Mixtral_FT"
output_path = "../../results/LLM/fig_levenshtein_distance_GPT.png"

build_levenshtein_boxplot(config, title, output_path)