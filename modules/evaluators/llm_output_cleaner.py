import json, re

#path = "../../results/LLM/Mistral-7b/MISTRAL_zero_shot_"

#path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_zero_shot_"

#path = "../../results/LLM/Mistral-7b/MISTRAL_few_shot_"

#path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_few_shot_"

#path = "../../results/LLM/Mistral-7b/MISTRAL_fine_tuned_"

#path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_fine_tuned_"

#path = "../../results/LLM/Miqu-1-70b/MIQU_zero_shot_"

#path = "../../results/LLM/Miqu-1-70b/MIQU_few_shot_"

path = "../../results/LLM/Miqu-1-70b/MIQU_fine_tuned_"

with open(path+"raw_answers.json") as file:
    data = json.load(file)

output = {}

regex = r"{.*?}"
for sentence, llm_output in data.items():
    matches = re.search(regex, llm_output, re.DOTALL)
    if matches:
        try:
            json_loaded = json.loads(matches[0])
            json_output = {}

            if "Action" in json_loaded:
                json_output["action"] = json_loaded.pop("Action")
            if "Acteur" in json_loaded:
                json_output["actor"] = json_loaded.pop("Acteur")
            if "Objet" in json_loaded:
                json_output["artifact"] = json_loaded.pop("Objet")
            if "Condition" in json_loaded:
                json_output["condition"] = json_loaded.pop("Condition")
            if "Lieu" in json_loaded:
                json_output["location"] = json_loaded.pop("Lieu")
            if "Modalité" in json_loaded:
                json_output["modality"] = json_loaded.pop("Modalité")
            if "Référence" in json_loaded:
                json_output["reference"] = json_loaded.pop("Référence")
            if "Temps" in json_loaded:
                json_output["time"] = json_loaded.pop("Temps")

            output[sentence] = json_output
        except Exception as e:
            print(f"can't transform string to json for sentence : {sentence}")
            continue

with open(path+"cleaned.json", 'w', encoding='utf-8') as file:
    json.dump(output, file)