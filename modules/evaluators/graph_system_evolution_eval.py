import json
import numpy as np
import matplotlib.pyplot as plt


def graph_system_evolution_eval(config, title, output_path):
    bars = []

    for i in range(len(config)):
        bars.append([])

    for i in range(len(config)):
        sum_perfect_equals = 0
        sum_subpart = 0
        sum_miss_classification = 0
        sum_hallucination = 0

        with open(config[i]['new']) as file:
            data = json.load(file)
            data = data["break_down"]
            for tag, values in data.items():
                sum_perfect_equals += values['perfect_equals']
                sum_subpart += values['subpart']
                sum_miss_classification += values['miss_classification']
                sum_hallucination += values['hallucination']

        with open(config[i]['ref']) as file:
            data = json.load(file)
            data = data["break_down"]
            for tag, values in data.items():
                sum_perfect_equals -= values['perfect_equals']
                sum_subpart -= values['subpart']
                sum_miss_classification -= values['miss_classification']
                sum_hallucination -= values['hallucination']

        bars[i].append(sum_perfect_equals)
        bars[i].append(sum_subpart)
        bars[i].append(sum_miss_classification)
        bars[i].append(sum_hallucination)

    # set width of bars
    barWidth = 0.25

    r_list = [np.arange(len(bars[0]))]
    # Set position of bar on X axis
    for i in range(1, len(config)):
        r_list.append([x + barWidth for x in r_list[i-1]])

    # Make the plot
    for i in range(len(config)):
        plt.bar(r_list[i], bars[i], color=config[i]['color'], width=barWidth, edgecolor='white', label=config[i]['title'])

    # Add xticks on the middle of the group bars
    plt.ylabel('Number of predicate')
    plt.xlabel(title, fontweight='bold')
    plt.xticks([r + barWidth for r in range(len(bars[0]))],
               ['Perfect equals', 'Subpart', 'Miss classification', 'Others'])

    # Create legend & Show graphic
    plt.legend()
    plt.savefig(output_path)


config = [
    {
        'ref': '../../results/LLM/Mistral-7b/MISTRAL_zero_shot_results.json',
        'new': '../../results/LLM/Mistral-7b/MISTRAL_fine_tuned_results.json',
        'title': 'Mistral-7b',
        'color': '#5619d8'
    }
]

title = "Evolution from Zero-shot to Fine-tuning predicate extraction with Mistral"
output_path = "../../results/LLM/few_shot_to_fine_tuning_mistral_evolution.png"

graph_system_evolution_eval(config, title, output_path)