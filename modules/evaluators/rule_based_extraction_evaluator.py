from itertools import combinations
import plotly.graph_objects as go
from math import comb
import random

class RuleBasedExtractionEvaluator:

    def evaluate(self, E_augmented = {}, E_goldStandard = {}, fileName = "test", sampling_trigger = 50000):
        E_goldStandard = set(E_goldStandard)

        data = []
        data_precision = []
        data_recall = []

        for i in range(1, len(E_augmented)):

            currentData = []
            currentData_precision = []
            currentData_recall = []

            if comb(len(E_augmented), i) < sampling_trigger * 100:
                combinaisons = list(combinations(E_augmented.keys(), i))
                if len(combinaisons) > sampling_trigger:
                    combinaisons = random.sample(combinaisons, sampling_trigger)
            else:
                combinaisons_uniques = set()
                while len(combinaisons_uniques) < sampling_trigger:
                    combinaison = tuple(random.sample(list(E_augmented.keys()), i))
                    combinaisons_uniques.add(combinaison)

                # Convertir en liste pour l'affichage
                combinaisons = list(combinaisons_uniques)

            for combinaison in combinaisons:

                merge = set()
                for element in combinaison:
                    merge = merge | set(E_augmented[element])

                fn = len(E_goldStandard - merge)
                tp = len(E_goldStandard & merge)
                fp = len(merge - E_goldStandard)

                currentData.append(tp)
                currentData_recall.append(tp / (tp + fn))

                if tp + fp != 0:
                    currentData_precision.append(
                        tp / (tp + fp)
                    )

            data.append(go.Box(
                y=currentData,
                marker_color='#2B3499',
                text=["test"],
                name=f"{i} marker"
            ))

            data_recall.append(go.Box(
                y=currentData_recall,
                marker_color='#2B3499',
                text=["test"],
                name=f"{i} marker"
            ))

            data_precision.append(go.Box(
                y=currentData_precision,
                marker_color='#2B3499',
                text=["test"],
                name=f"{i} marker"
            ))

        fig = go.Figure(data)
        fig.update_layout(
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
            yaxis=dict(zeroline=False, gridcolor='white'),
            paper_bgcolor='rgb(233,233,233)',
            plot_bgcolor='rgb(233,233,233)',
            showlegend=False
        )

        fig_recall = go.Figure(data_recall)
        fig.update_layout(
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
            yaxis=dict(zeroline=False, gridcolor='white'),
            paper_bgcolor='rgb(233,233,233)',
            plot_bgcolor='rgb(233,233,233)',
            showlegend=False
        )

        fig_precision = go.Figure(data_precision)
        fig.update_layout(
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
            yaxis=dict(zeroline=False, gridcolor='white'),
            paper_bgcolor='rgb(233,233,233)',
            plot_bgcolor='rgb(233,233,233)',
            showlegend=False
        )

        #fig.write_image(f"./results/{fileName}.png", format='png', engine='kaleido')
        fig_recall.write_image(f"{fileName}_recall.png", format='png', engine='kaleido')
        fig_precision.write_image(f"{fileName}_precision.png", format='png', engine='kaleido')

        #éléments non trouvés
        merge = set()
        for element in E_augmented:
            merge = merge | set(E_augmented[element])

        not_found = E_goldStandard - merge
        print(not_found)

        return


