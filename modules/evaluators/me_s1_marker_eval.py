from modules.evaluators.recall_precision_f1_f2 import recall_precision_f1_f2
from modules.marker_enhancer.s1 import MarkerEnhancerSemanticRessources
from data.MarkersList import MarkersList
from modules.neo4j.PatternsRequest import PatternsRequest
import matplotlib.pyplot as plt
import numpy as np

top_setting = 5


def save_graph(base_system_performance, improvement_degradation, categories, metrics):
    n_metrics = len(metrics)

    # Créer le barplot
    fig, ax = plt.subplots(figsize=(15, 8))

    bar_width = 0.25
    index = np.arange(len(categories))

    # Couleurs pour les barres en fonction de l'amélioration ou de la dégradation
    colors = [['green' if diff > 0 else 'red' for diff in cat_diff] for cat_diff in improvement_degradation]

    # Tracer les barres pour chaque métrique
    for i, metric in enumerate(metrics):
        bars1 = ax.bar(index + i * bar_width, base_system_performance[:, i], bar_width, label=f'{metric}', alpha=0.7, color='lightgray')
        bars2 = ax.bar(index + i * bar_width, improvement_degradation[:, i], bar_width, bottom=base_system_performance[:, i], color=[colors[j][i] for j in range(len(categories))], alpha=0.7)

        # Ajouter les valeurs sur les barres du système 1
        for bar1, perf1 in zip(bars1, base_system_performance[:, i]):
            height = bar1.get_height()
            ax.annotate(f'{perf1:.2f}',
                        xy=(bar1.get_x() + bar1.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points de décalage vertical
                        textcoords="offset points",
                        ha='center', va='bottom', color='blue')

        # Ajouter les valeurs sur les barres du système 2
        for bar1, bar2, perf1, diff in zip(bars1, bars2, base_system_performance[:, i], improvement_degradation[:, i]):
            height = bar1.get_height() + bar2.get_height()
            ax.annotate(f'{perf1 + diff:.2f}',
                        xy=(bar2.get_x() + bar2.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points de décalage vertical
                        textcoords="offset points",
                        ha='center', va='bottom', color='black')

    # Ajouter des labels et une légende
    ax.set_xlabel('Catégories')
    ax.set_ylabel('Performance')
    ax.set_title('Performance du Système avec Améliorations/Dégradations')
    ax.set_xticks(index + bar_width)
    ax.set_xticklabels(categories)
    ax.legend()

    # Enregistrer le graphique dans une image
    plt.savefig(f'./results/S1/augmentation_performance_top{top_setting}.png', bbox_inches='tight')
    plt.close(fig)


def main():
    markerList = {
        "location": MarkersList().getLocationMarkers(),
        "exception": MarkersList().getExceptionMarkers(),
        "condition": MarkersList().getConditionMarkers(),
        "actor": MarkersList().getActorMarkers(),
        "modality": MarkersList().getModalityMarkers(),
        "time": MarkersList().getTimeMarkers(),
        "violation": MarkersList().getViolationMarkers(),
        "situation": MarkersList().getSituationMarkers(),
        "sanction": MarkersList().getSanctionMarkers(),
        "artifact": MarkersList().getArtifactMarkers(),
    }

    final_results_base = []
    final_results_augmented = []

    augmentationMarkersStorage = {}
    fullMarkersStorage = {}

    for key, value in markerList.items():
        print(f"================= {key} =================")

        # ==========================
        #         Markers
        # ==========================
        mkhsr = MarkerEnhancerSemanticRessources(value).exec(lemma_setting=True, top_setting=top_setting)
        augmentationMarkersStorage[key] = mkhsr.getAugmentedMarkers()
        fullMarkersStorage[key] = {next(iter(mkhsr.getFullMarkers())): mkhsr.getFullMarkers()}

        # ==========================
        #         Neo4j
        # ==========================
        E_fullMarkers = PatternsRequest().request(key, fullMarkersStorage)
        E_augmented = PatternsRequest().request(key, augmentationMarkersStorage) | E_fullMarkers
        E_goldStandard = PatternsRequest().getGoldStandard(key)

        fn = len(E_goldStandard - E_fullMarkers)
        tp = len(E_goldStandard & E_fullMarkers)
        fp = len(E_fullMarkers - (E_goldStandard & E_fullMarkers))
        result = recall_precision_f1_f2(fn, tp, fp)
        final_results_base.append([result['recall'], result['precision'], result['f1']])

        fn = len(E_goldStandard - E_augmented)
        tp = len(E_goldStandard & E_augmented)
        fp = len(E_augmented - (E_goldStandard & E_augmented))
        result = recall_precision_f1_f2(fn, tp, fp)
        final_results_augmented.append([result['recall'], result['precision'], result['f1']])

    final_results_base = np.array(final_results_base)
    final_results_augmented = np.array(final_results_augmented)
    delta = final_results_augmented - final_results_base

    save_graph(final_results_base, delta, list(markerList.keys()), ['recall', 'precision', 'f1'])

if __name__ == '__main__':
    main()
