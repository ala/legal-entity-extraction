import random
import time
from itertools import combinations
from math import comb
import numpy as np
import plotly.graph_objects as go
import statistics
from alive_progress import alive_bar
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd


class MarkerEvaluator:
    '''
    This marker evaluator allow us to benchmark the marker augmentation of the system.
    @input :
        * fileName : root name of the files to be saved
        * fullMarkers : list with all the markers
        * augmentedMarkers : for each marker we have the store the augmented markers
    @ouput :
        * <fileName>_chart_solutions.png : permet de
    '''

    def boxPlot(fileName, fullMarkers, augmentedMarkers, sampling_trigger):
        data_precision = []
        maximum_value_precision = 0
        maximum_value_step_precision = 1
        optimal_median_value_precision = 0
        optimal_median_value_step_precision = 1
        standard_deviation_precision = []

        data_recall = []
        maximum_value_recall = 0
        maximum_value_step_recall = 1
        optimal_median_value_recall = 0
        optimal_median_value_step_recall = 1
        standard_deviation_recall = []

        len_markers = len(fullMarkers)

        # X axis
        for i in range(1, len_markers):
            print(f"{i}/{len_markers}")

            currentData_precision = []
            currentData_recall = []

            # Y axis
            if comb(len_markers, i) < sampling_trigger * 100:
                combinaisons = list(combinations(fullMarkers, i))
                if len(combinaisons) > sampling_trigger:
                    combinaisons = random.sample(combinaisons, sampling_trigger)
            else:
                combinaisons_uniques = set()
                while len(combinaisons_uniques) < sampling_trigger:
                    combinaison = tuple(random.sample(fullMarkers, i))
                    combinaisons_uniques.add(combinaison)

                # Convertir en liste pour l'affichage
                combinaisons = list(combinaisons_uniques)

            with alive_bar(len(combinaisons)) as bar:
                for combinaison in combinaisons:

                    merge = set()
                    for element in combinaison:
                        merge = merge | augmentedMarkers[element]

                    fn = len(set(fullMarkers) - merge)
                    tp = len((set(fullMarkers) & merge) - set(combinaison))
                    fp = len(merge - set(fullMarkers))

                    recall = tp / (fn + tp)
                    if tp + fp != 0:
                        precision = tp / (tp + fp)
                    else:
                        precision = 0

                    currentData_recall.append(recall)
                    currentData_precision.append(precision)

                    if recall > maximum_value_recall:
                        maximum_value_recall = recall
                        maximum_value_step_recall = i

                    if precision > maximum_value_precision:
                        maximum_value_precision = precision
                        maximum_value_step_precision = i

                    bar()

            # Calculus
            median_recall = statistics.median(currentData_recall)
            median_precision = statistics.median(currentData_precision)

            if median_precision > optimal_median_value_precision:
                optimal_median_value_precision = median_precision
                optimal_median_value_step_precision = i

            if median_recall > optimal_median_value_recall:
                optimal_median_value_recall = median_recall
                optimal_median_value_step_recall = i

            standard_deviation_precision.append(statistics.stdev(currentData_precision))
            standard_deviation_recall.append(statistics.stdev(currentData_recall))

            data_recall.append(go.Box(
                y=currentData_recall,
                marker_color='#2B3499',
                text=["test"],
                name=f"{i} marker"
            ))

            data_precision.append(go.Box(
                y=currentData_precision,
                marker_color='#2B3499',
                text=["test"],
                name=f"{i} marker"
            ))

        # Generating graph
        fig_recall = go.Figure(data_recall)
        fig_recall.update_layout(
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
            yaxis=dict(zeroline=False, gridcolor='white'),
            paper_bgcolor='rgb(233,233,233)',
            plot_bgcolor='rgb(233,233,233)',
            showlegend=False
        )

        fig_precision = go.Figure(data_precision)
        fig_precision.update_layout(
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=True),
            yaxis=dict(zeroline=False, gridcolor='white'),
            paper_bgcolor='rgb(233,233,233)',
            plot_bgcolor='rgb(233,233,233)',
            showlegend=False
        )

        fig_recall.write_image(f"{fileName}_recall.png", format='png', engine='kaleido')
        fig_precision.write_image(f"{fileName}_precision.png", format='png', engine='kaleido')

        print(f'''
        ========= {fileName} =========
        R | P | F1
        Nombre de marqueurs : {len_markers}
        Valeur maximum : {round(maximum_value_recall, 2)} ({maximum_value_step_recall}) | {round(maximum_value_precision, 2)} ({maximum_value_step_precision})
        Valeur optimale médiane : {round(optimal_median_value_recall, 2)} ({optimal_median_value_step_recall}) | {round(optimal_median_value_precision, 2)} ({optimal_median_value_step_precision})
        écart type de l'optimal : {round(standard_deviation_recall[optimal_median_value_step_recall - 1], 2)} | {round(standard_deviation_recall[optimal_median_value_step_precision - 1], 2)}
        écart type minimum : {round(min(standard_deviation_recall), 2)} ({standard_deviation_recall.index(min(standard_deviation_recall)) + 1}) | {round(min(standard_deviation_precision), 2)} ({standard_deviation_precision.index(min(standard_deviation_precision)) + 1})
        écart type maximum : {round(max(standard_deviation_recall), 2)} ({standard_deviation_recall.index(max(standard_deviation_recall)) + 1}) | {round(max(standard_deviation_precision), 2)} ({standard_deviation_precision.index(max(standard_deviation_precision)) + 1})
        écart type médian : {round(statistics.median(standard_deviation_recall), 2)} | {round(statistics.median(standard_deviation_precision), 2)}
        ''')

        return

    def heathMap(fileName, fullMarkers, augmentedMarkers):
        len_markers = len(fullMarkers)
        true_positive_matrix = np.zeros((len_markers, len_markers))

        for fullMarker in fullMarkers:
            for augmentedMarker in augmentedMarkers[fullMarker]:
                if augmentedMarker in fullMarkers:
                    true_positive_matrix[fullMarkers.index(fullMarker), fullMarkers.index(augmentedMarker)] = 1

        df = pd.DataFrame(true_positive_matrix, index=fullMarkers, columns=fullMarkers)

        plt.figure(figsize=(11, 11))
        ax = sns.heatmap(df, linewidth=1, cbar=False)
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor",fontsize=12)
        plt.savefig(f"{fileName}/heatmap.png", dpi=300)

        return
