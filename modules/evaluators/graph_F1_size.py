import plotly.graph_objects as go

# Exemple de données
x = [7, 46.7, 70, 1800]  # Axe des X
x_rev = x[::-1]

y = [0.448, 0.526, 0.6, 0.69]  # Axe des Y
y_lower = [0.165, 0.514, 0.576, 0.664]
y_lower = y_lower[::-1]

names = ['Mistral-7b', 'Mixtral-8x7b', 'Miqu-1-70b', 'GPT-4']

fig = go.Figure()

# Créer le graphique en ligne

# fig.add_trace(go.Scatter(
#     x=x+x_rev,
#     y=y+y_lower,
#     fill='toself',
#     fillcolor='rgba(231,107,243,0.2)',
#     line_color='rgba(255,255,255,0)',
#     showlegend=False,
# ))

fig.add_trace(go.Scatter(
    x=x, y=y,
    line_color='rgb(231,107,243)',
    name='Max F1 Score',
    mode='lines+markers',
    showlegend=True,
))


fig.add_annotation(x=x[0], y=y[0], text=names[0], showarrow=False, yshift=-10, xshift=40)
fig.add_annotation(x=x[1], y=y[1], text=names[1], showarrow=False, yshift=0, xshift=40)
fig.add_annotation(x=x[2], y=y[2], text=names[2], showarrow=False, yshift=20, xshift=20)
fig.add_annotation(x=x[3], y=y[3], text=names[3], showarrow=False, yshift=20, xshift=-20)

fig.update_yaxes(range=[0, 1])
fig.update_xaxes(range=[0, None])

fig.update_layout(
    xaxis=dict(
        range=[-50, 2000]  # Ajustement des limites avec la marge
    ),
    xaxis_title="Model size in billions of parameters",
    yaxis_title="F1 score"
)

# Afficher le graphique
fig.write_image('../../results/LLM/fig_f1_on_size.png', format='png', engine='kaleido', scale=4)
