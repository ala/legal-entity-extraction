import json, re

input_path = "../../results/LLM/GPT-4/GPT-4_zero_shot_v2_raw_answers.json"
output_path = "../../results/LLM/GPT-4/GPT-4_zero_shot_v2_cleaned.json"

#input_path = "../../results/LLM/Mistral-7b/MISTRAL_zero_shot_raw_answers.json"
#output_path = "../../results/LLM/Mistral-7b/MISTRAL_zero_shot_cleaned.json"

#input_path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_zero_shot_raw_answers.json"
#output_path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_zero_shot_cleaned.json"

#input_path = "../../results/LLM/Mistral-7b/MISTRAL_few_shot_raw_answers.json"
#output_path = "../../results/LLM/Mistral-7b/MISTRAL_few_shot_cleaned.json"

#input_path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_few_shot_raw_answers.json"
#output_path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_few_shot_cleaned.json"

#input_path = "../../results/LLM/Mistral-7b/MISTRAL_fine_tuned_raw_answers.json"
#output_path = "../../results/LLM/Mistral-7b/MISTRAL_fine_tuned_cleaned.json"

#input_path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_fine_tuned_raw_answers.json"
#output_path = "../../results/LLM/Mixtral-8x7b/MIXTRAL_fine_tuned_cleaned.json"

#input_path = "../../results/LLM/Miqu-1-70b/MIQU_zero_shot_raw_answers.json"
#output_path = "../../results/LLM/Miqu-1-70b/MIQU_zero_shot_cleaned.json"

with open(input_path) as file:
    data = json.load(file)

output = {}

regex = r"(Action|Acteur|Objet|Condition|Lieu|Modalité|Référence|Temps)\s*:\s*(\[.*])"

for sentence, llm_output in data.items():
    lines = llm_output.split('\n')
    json_output = {}
    for line in lines:
        matches = re.search(regex, line, re.DOTALL)
        raw_array = matches[2]
        if matches:
            try:
                #Clean simple quotes
                for index in range(len(raw_array)):
                    char = raw_array[index]

                    if char != "'":
                        continue

                    if raw_array[index-1] == "[":
                        raw_array = raw_array[:index] + "\"" + raw_array[index + 1:]
                        continue
                    if raw_array[index+1] == "]" or raw_array[index+1] == ",":
                        raw_array = raw_array[:index] + "\"" + raw_array[index + 1:]
                        continue
                    if index > 2 and (raw_array[index-2] + raw_array[index-1]) == ", ":
                        raw_array = raw_array[:index] + "\"" + raw_array[index + 1:]
                        continue



                json_loaded = json.loads("{ \"tab\" :" + raw_array + "}")

                if "Action" in matches[1]:
                    json_output["action"] = json_loaded['tab']
                if "Acteur" in matches[1]:
                    json_output["actor"] = json_loaded['tab']
                if "Objet" in matches[1]:
                    json_output["artifact"] = json_loaded['tab']
                if "Condition" in matches[1]:
                    json_output["condition"] = json_loaded['tab']
                if "Lieu" in matches[1]:
                    json_output["location"] = json_loaded['tab']
                if "Modalité" in matches[1]:
                    json_output["modality"] = json_loaded['tab']
                if "Référence" in matches[1]:
                    json_output["reference"] = json_loaded['tab']
                if "Temps" in matches[1]:
                    json_output["time"] = json_loaded['tab']

            except Exception as e:
                print(f"can't transform string to json for sentence : {raw_array} | {e}")
                continue

    output[sentence] = json_output

with open(output_path, 'w', encoding='utf-8') as file:
    json.dump(output, file)