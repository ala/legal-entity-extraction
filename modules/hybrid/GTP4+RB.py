import json
import numpy as np
from alive_progress import alive_bar
from modules.evaluators.recall_precision_f1_f2 import recall_precision_f1_f2
from modules.marker_enhancer.s1 import MarkerEnhancerSemanticRessources
from data.MarkersList import MarkersList
from modules.neo4j.RefinedRequest import RefinedRequest
import matplotlib.pyplot as plt


def save_graph(base_system_performance, improvement_degradation, categories, metrics, base_colors):
    n_metrics = len(metrics)
    n_categories = len(categories)

    # Créer le barplot
    fig, ax = plt.subplots(figsize=(15, 8))

    bar_width = 0.25
    index = np.arange(n_categories)

    # Couleurs pour les barres en fonction de l'amélioration ou de la dégradation
    colors = [['green' if diff > 0 else 'red' for diff in cat_diff] for cat_diff in improvement_degradation]

    # Tracer les barres pour chaque métrique
    for i, metric in enumerate(metrics):
        bars1 = ax.bar(index + i * bar_width, base_system_performance[:, i], bar_width, label=f'{metric}', alpha=0.7,
                       color=base_colors[i])
        bars2 = ax.bar(index + i * bar_width, improvement_degradation[:, i], bar_width,
                       bottom=base_system_performance[:, i], color=[colors[j][i] for j in range(n_categories)],
                       alpha=0.7)

        # Ajouter les valeurs sur les barres du système 1
        for bar1, perf1 in zip(bars1, base_system_performance[:, i]):
            height = bar1.get_height()
            ax.annotate(f'{perf1:.2f}',
                        xy=(bar1.get_x() + bar1.get_width() / 2, height),
                        xytext=(0, 0),  # 3 points de décalage vertical
                        textcoords="offset points",
                        ha='center', va='bottom', color='blue')

        # Ajouter les valeurs sur les barres du système 2
        for bar1, bar2, perf1, diff in zip(bars1, bars2, base_system_performance[:, i], improvement_degradation[:, i]):
            height = bar1.get_height() + bar2.get_height()
            ax.annotate(f'{perf1 + diff:.2f}',
                        xy=(bar2.get_x() + bar2.get_width() / 2, height),
                        xytext=(0, 0),  # Increased vertical offset to 10 points
                        textcoords="offset points",
                        ha='center', va='bottom', color='black')

    # Ajouter des labels et une légende
    ax.set_xlabel('Categories')
    ax.set_ylabel('Performance')
    #ax.set_title('Performance du Système avec Améliorations/Dégradations')
    ax.set_xticks(index + bar_width * (n_metrics - 1) / 2)
    ax.set_xticklabels(categories)
    ax.legend()

    ax.set_ylim(0, 1)

    # Enregistrer le graphique dans une image
    plt.savefig(f'../../results/Hybrid/performance.png', bbox_inches='tight')
    plt.close(fig)


def reverseStanzaParsing(text):
    if "de le" in text:
        text = text.replace("de le ", "du ")
    if "de les" in text:
        text = text.replace("de les ", "des ")
    if "à le" in text:
        text = text.replace("à le ", "au ")
    if "à les" in text:
        text = text.replace("à les ", "aux ")
    return text

def separate_intervals(data):
    # Trier la liste
    sorted_data = sorted(data, key=lambda x: int(x.split('.')[-1]))

    separated_intervals = []
    current_interval = [sorted_data[0]]

    for i in range(1, len(sorted_data)):
        current_value = int(sorted_data[i].split('.')[-1])
        previous_value = int(current_interval[-1].split('.')[-1])

        if current_value == previous_value + 1:
            current_interval.append(sorted_data[i])
        else:
            separated_intervals.append(current_interval)
            current_interval = [sorted_data[i]]

    separated_intervals.append(current_interval)  # Ajouter le dernier intervalle

    return separated_intervals

def evaluation(LLM_ExtractionByConcept, LLM_RB_ExtractionByConcept, concepts, neo4j):
    final_results_base = []
    final_results_refined = []
    for key, value in LLM_ExtractionByConcept.items():
        if key not in ['action', 'actor', 'artifact', 'condition', 'location', 'modality', 'reference', 'time']:
            continue
        else:
            concepts.append(key)

        E_goldStandard = neo4j.getGoldStandard(key)
        LLM_annotations = LLM_ExtractionByConcept[key]
        LLM_RB_annotations = LLM_RB_ExtractionByConcept[key]

        fn1 = len(E_goldStandard - LLM_annotations)
        tp1 = len(E_goldStandard & LLM_annotations)
        fp1 = len(LLM_annotations - (E_goldStandard & LLM_annotations))
        result1 = recall_precision_f1_f2(fn1, tp1, fp1)
        final_results_base.append([result1['recall'], result1['precision'], result1['f1']])

        fn2 = len(E_goldStandard - LLM_RB_annotations)
        tp2 = len(E_goldStandard & LLM_RB_annotations)
        fp2 = len(LLM_RB_annotations - (E_goldStandard & LLM_RB_annotations))
        result2 = recall_precision_f1_f2(fn2, tp2, fp2)
        final_results_refined.append([result2['recall'], result2['precision'], result2['f1']])

        print("faux négatifs", list((E_goldStandard - LLM_RB_annotations) - (E_goldStandard - LLM_annotations)))

        final_results_base = np.array(final_results_base)
        final_results_refined = np.array(final_results_refined)
        delta = final_results_refined - final_results_base

        save_graph(final_results_base, delta, concepts, ['1- recall', '2- precision', '3- f1'],
                   ['lightgrey', 'silver', 'darkgrey'])


def main():
    neo4j = RefinedRequest()

    concepts = []

    output = {}

    LLM_ExtractionByConcept = {
        "location": set(),
        "exception": set(),
        "condition": set(),
        "actor": set(),
        "modality": set(),
        "time": set(),
        "violation": set(),
        "situation": set(),
        "sanction": set(),
        "artifact": set(),
        "reason": set(),
        "action": set(),
        "reference": set(),
        "constraint": set(),
    }

    LLM_RB_ExtractionByConcept = {
        "location": set(),
        "exception": set(),
        "condition": set(),
        "actor": set(),
        "modality": set(),
        "time": set(),
        "violation": set(),
        "situation": set(),
        "sanction": set(),
        "artifact": set(),
        "reason": set(),
        "action": set(),
        "reference": set(),
        "constraint": set(),
    }

    with open('../../results/LLM/GPT-4/GPT-4_few_shot_cleaned_refined.json', 'r', encoding='utf-8') as file:
        loaded = json.load(file)

    with alive_bar(len(loaded)) as bar:
        index = 207
        for sentence in loaded:
            reverseSentence = reverseStanzaParsing(sentence)
            output[reverseSentence] = {}
            for concept, annotations in loaded[sentence].items():
                for annotation in annotations:
                    original, refined = neo4j.request(index, concept, annotation)
                    LLM_ExtractionByConcept[concept].update(original)
                    LLM_RB_ExtractionByConcept[concept].update(refined)

                    if len(refined) == 0:
                        continue

                    if concept not in output[reverseSentence]:
                        output[reverseSentence][concept] = []

                    for interval in separate_intervals(list(refined)):

                        span = reverseStanzaParsing(neo4j.getSpan(interval).strip())

                        if span in output[reverseSentence][concept]:
                            continue
                        if span == "":
                            print(f"=== Erreur span null ===\n ➡️{index}\n ➡️{concept}\n ➡️{annotation}\n ➡️{list(refined)}\n ➡️{reverseSentence}\n")
                        output[reverseSentence][concept].append(span)

            index += 1
            bar()

    #=========================
    #       Evaluation
    #=========================
    #evaluation(LLM_ExtractionByConcept, LLM_RB_ExtractionByConcept, concepts, neo4j)

    #=========================
    #       Exportation
    #=========================
    with open('../../results/Hybrid/GPT4+RB.json', 'w', encoding='utf-8') as file:
        json.dump(output, file)

if __name__ == '__main__':
    main()
