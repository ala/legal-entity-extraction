import itertools

from neo4j import GraphDatabase


class PatternsRequest:
    driver = None

    def __init__(self):
        uri = "bolt://localhost:7687"  # Modifier l'URI en fonction de votre configuration
        username = "neo4j"
        password = "password"
        self.driver = GraphDatabase.driver(uri, auth=(username, password))

    def request(self, type, markers):
        if type == "location":
            return self.location(markers[type])
        elif type == "modality":
            return self.modality(markers[type])
        elif type == "reason":
            return self.reason(markers[type])
        elif type == "time":
            return self.time(markers[type])
        elif type == "condition":
            return self.condition(markers[type], exceptionMarkers=markers["exception"])
        elif type == "exception":
            return self.exception(markers[type])
        elif type == "actor":
            return self.actor(markers[type])
        elif type == "violation":
            return self.violation(markers[type])
        elif type == "situation":
            return self.violation(markers[type])
        elif type == "sanction":
            return self.violation(markers[type])
        elif type == "artifact":
            return self.artifact(
                artifact=markers["artifact"],
                violation=markers["violation"],
                time=markers["time"],
                situation=markers["situation"],
                sanction=markers["sanction"],
                location=markers["location"],
                actor=markers["actor"]
            )
        else:
            return set()

    def location(self, markers):
        output = {}
        for key in markers:
            simpleToken, multiToken = self.splitMultiTokens(list(markers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            nodes = self.driver.execute_query(
                '''
                match (c:Constituent)-[:CONSREL*..2]->(w:Word)
                where c.type = "NP"
                and w.id in $array
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                ''',
                array=list(listMergedToken)
            )
            output[key] = [record[0] for record in nodes.records]

        return set(itertools.chain.from_iterable(output.values()))

    def modality(self, markers):
        output = {}
        for key in markers:

            simpleToken, multiToken = self.splitMultiTokens(list(markers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            nodes = self.driver.execute_query(
                '''
                match (c:Constituent)-[:CONSREL]->(w:Word)
                where c.type = "VN"
                and w.id in $array
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                
                UNION
                
                match (c:Constituent)-[:CONSREL]->(w:Word)
                where c.type = "SENT"
                and toLower(w.text) in $array
                return w.id
                ''',
                array=list(listMergedToken)
            )
            output[key] = [record[0] for record in nodes.records]

        return set(itertools.chain.from_iterable(output.values()))

    def reason(self, markers):
        output = {}
        for key in markers:

            simpleToken, multiToken = self.splitMultiTokens(list(markers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            nodes = self.driver.execute_query(
                '''            
                match (c:Constituent)-[:CONSREL*..]->(w:Word)
                where w.id in $array
                and c.type in ["Srel", "Ssub", "PP"]
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                
                UNION
                
                match (c:Constituent{type:"NP"})-[:CONSREL*..]->(c1:Constituent{type:"P+"})-[:CONSREL]->(w:Word)
                match (c1)<-[:CONSREL]-(:Constituent)-[:CONSREL]->(:Constituent{type: "VPinf"})
                where w.id in $array
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                
                UNION
                
                match (c:Constituent{type:"NP"})-[:CONSREL]->(c1:Constituent{type:"VPpart"})-[:CONSREL*..]->(w:Word)
                where w.id in $array
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                ''',
                array=list(listMergedToken)
            )
            output[key] = [record[0] for record in nodes.records]

        return set(itertools.chain.from_iterable(output.values()))

    def time(self, markers):
        output = {}

        for key in markers:

            simpleToken, multiToken = self.splitMultiTokens(list(markers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            nodes = self.driver.execute_query(
                '''            
                match (c:Constituent)-[:CONSREL]->(w:Word)
                where c.type = "NP"
                and w.id in $array
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                
                UNION
                
                match (c1:Constituent {type: "PP"})-[:CONSREL]->(c2:Constituent {type: "P+"})-[:CONSREL]->(w:Word)
                match (c2)<-[:CONSREL]-(:Constituent)-[:CONSREL]->(:Constituent {type: "NP"})
                where w.id in $array 
                with c1 as c1
                match (c1)-[:CONSREL*..]->(w:Word)
                return w.id
                ''',
                array=list(listMergedToken)
            )
            output[key] = [record[0] for record in nodes.records]

        return set(itertools.chain.from_iterable(output.values()))

    def violation(self, markers):
        output = {}

        for key in markers:

            simpleToken, multiToken = self.splitMultiTokens(list(markers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            nodes = self.driver.execute_query(
                '''            
                match (c:Constituent)-[:CONSREL]->(w:Word)
                where c.type = "NP"
                and w.id in $array
                with c as constituent
                match (constituent)-[:CONSREL*..]->(w:Word)
                return w.id
                ''',
                array=list(listMergedToken)
            )
            output[key] = [record[0] for record in nodes.records]

        return set(itertools.chain.from_iterable(output.values()))

    def exception(self, exceptionMarkers):
        output = {}
        for key in exceptionMarkers:
            simpleToken, multiToken = self.splitMultiTokens(list(exceptionMarkers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            result = set()
            if len(listMergedToken) > 0:
                result = self.driver.execute_query(
                    '''
                    match (c:Constituent)-[:CONSREL*..2]->(w:Word)
                    where w.id in $exceptionMarkers
                    and c.type in ["Srel", "PP", "Ssub"]
                    with c as constituent
                    match (constituent)-[:CONSREL*..]->(w:Word)
                    return w.id
                    
                    UNION
                    
                    match (c:Constituent{type:"NP"})-[:CONSREL*..]->(c1:Constituent{type:"P+"})-[:CONSREL]->(w:Word)
                    match (c1)<-[:CONSREL]-(:Constituent)-[:CONSREL]->(c2:Constituent{type: "VPinf"})
                    where w.id in $exceptionMarkers
                    with c2 as constituent
                    match (constituent)-[:CONSREL*..]->(w:Word)
                    return w.id
                    
                    UNION
                    
                    match (c:Constituent{type:"NP"})-[:CONSREL]->(c1:Constituent{type:"VPpart"})-[:CONSREL*..]->(w:Word)
                    where w.id in $exceptionMarkers
                    with c1 as constituent
                    match (constituent)-[:CONSREL*..]->(w:Word)
                    return w.id
                    ''',
                    exceptionMarkers=list(listMergedToken)
                )
                output[key] = [record[0] for record in result.records]

        return set(itertools.chain.from_iterable(output.values()))

    def condition(self, conditionMarkers, exceptionMarkers):
        output = {}

        markerBuffer = set()
        for markerType in [exceptionMarkers]:
            for _, values in markerType.items():
                markerBuffer = markerBuffer | set(values)
        simpleTokenBuffer, multiTokenBuffer = self.splitMultiTokens(list(markerBuffer))
        listMergedWrongToken = set(self.getIdMultiTokens(multiTokenBuffer)) | set(self.getIdSingleTokens(simpleTokenBuffer))

        for key in conditionMarkers:
            simpleToken, multiToken = self.splitMultiTokens(list(conditionMarkers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            result = set()
            if len(listMergedToken) > 0:
                result = self.driver.execute_query(
                    '''
                    match (c:Constituent)-[:CONSREL*..]->(w:Word)
                    where w.id in $conditionMarkers
                    and c.type in ["Srel", "PP"]
                    with c as constituent
                    match (constituent)-[:CONSREL*..]->(w:Word)
                    return w.id
                    
                    UNION
                    
                    match (c:Constituent)-[:CONSREL]->(w:Word)
                    where w.id in $conditionMarkers
                    and c.type in ["Ssub"]
                    with c as constituent
                    match (constituent)-[:CONSREL]->(w:Word)
                    return w.id
                    
                    UNION
                                        
                    match (c1:Constituent)-[:CONSREL]->(c2:Constituent)
                    WHERE NOT EXISTS {
                        MATCH (c2)-[:CONSREL*..]->(w:Word)
                        WHERE w.id in $exceptionMarker
                    }
                    and c1.type = "NP"
                    and c2.type in ["VPpart", "VPinf"]
                    with c2 as constituent
                    match (constituent)-[:CONSREL]->(w:Word)
                    where toInteger(split(w.id, '.')[0]) > 206
                    return w.id
                    ''',
                    conditionMarkers=list(listMergedToken),
                    exceptionMarker=list(listMergedWrongToken)
                )
                output[key] = [record[0] for record in result.records]

        return set(itertools.chain.from_iterable(output.values()))

    def actor(self, actorMarkers):
        output = {}
        for key in actorMarkers:
            simpleToken, multiToken = self.splitMultiTokens(list(actorMarkers[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            result = set()
            if len(listMergedToken) > 0:
                result = self.driver.execute_query(
                    '''
                    match (c:Constituent)-[:CONSREL]->(w:Word)
                    where w.id in $actorMarkers
                    and c.type in ["NP"]
                    with c as constituent
                    match (constituent)-[:CONSREL*..]->(w:Word)
                    return w.id
                    ''',
                    actorMarkers=list(listMergedToken)
                )
                output[key] = [record[0] for record in result.records]

        return set(itertools.chain.from_iterable(output.values()))

    def artifact(self, artifact, violation, time, situation, sanction, location, actor):
        output = {}

        markerBuffer = set()
        for markerType in [violation, time, situation, sanction, location, actor]:
            for _, values in markerType.items():
                markerBuffer = markerBuffer | set(values)
        simpleTokenBuffer, multiTokenBuffer = self.splitMultiTokens(list(markerBuffer))
        listMergedWrongToken = set(self.getIdMultiTokens(multiTokenBuffer)) | set(self.getIdSingleTokens(simpleTokenBuffer))

        for key in artifact:
            simpleToken, multiToken = self.splitMultiTokens(list(artifact[key]))
            listMergedToken = set(self.getIdMultiTokens(multiToken)) | set(self.getIdSingleTokens(simpleToken))

            result = set()
            if len(listMergedToken) > 0:
                result = self.driver.execute_query(
                    '''
                    match (c:Constituent)-[:CONSREL]->(w:Word)
                    where w.id in $artifactMarkers
                    and c.type in ["NP"]
                    with c as constituent
                    match (constituent)-[:CONSREL*..]->(w:Word)
                    return w.id
                    
                    UNION
                    
                    match (c1:Constituent)
                    WHERE NOT EXISTS {
                        MATCH (c1)-[:CONSREL*..]->(w:Word)
                        WHERE w.id in $exceptionMarker
                    }
                    and c1.type = "NP"
                    with c1 as constituent
                    match (constituent)-[:CONSREL]->(w:Word)
                    where toInteger(split(w.id, '.')[0]) > 206
                    return w.id
                    ''',
                    artifactMarkers=list(listMergedToken),
                    exceptionMarker=list(listMergedWrongToken)
                )
                output[key] = [record[0] for record in result.records]

        return set(itertools.chain.from_iterable(output.values()))

    def getGoldStandard(self, type="location"):
        nodes = self.driver.execute_query(
            '''
            match (c:Concept)--(w:Word)
            where toInteger(split(w.id, '.')[0]) > 206
            and c.type = $type
            return w.id
            ''',
            type=type
        )

        return set([record[0] for record in nodes.records])

    def splitMultiTokens(self, liste):
        simpleToken = set()
        multiToken = []

        for token in liste:
            if " " in token:
                multiToken.append(token.split(" "))
            else:
                simpleToken.add(token)

        return (simpleToken, multiToken)

    def getIdMultiTokens(self, multiTokens):
        listMultiToken = self.driver.execute_query(
            '''
            WITH $array AS liste
            unwind liste as words
            MATCH path = (start:Word)-[:NEXT*]->(end:Word)
            where size(words) - 1 = size(relationships(path))
            and toInteger(split(start.id, '.')[0]) > 206
            and all(
                idx IN range(0, size(words)-2)
                WHERE (
                    toLower(words[idx]) = toLower((nodes(path)[idx]).text)
                AND toLower(words[idx+1]) = toLower((nodes(path)[idx + 1]).text))
            )
            and start.text = words[0]
            and end.text = words[size(words) - 1]
            with nodes(path) as result
            unwind result as results
            return collect(results.id) as liste
            ''',
            array=multiTokens
        )
        return listMultiToken.records[0][0]

    def getIdSingleTokens(self, singleTokens):
        listSingleToken = self.driver.execute_query(
            '''
            match (w:Word)
            where toLower(w.text) in $array
            and toInteger(split(w.id, '.')[0]) > 206
            with distinct w as results
            return collect(results.id) as liste 
            ''',
            array=list(singleTokens)
        )
        return listSingleToken.records[0][0]
