from neo4j import GraphDatabase


class RefinedRequest:
    driver = None

    def __init__(self):
        uri = "bolt://localhost:7687"  # Modifier l'URI en fonction de votre configuration
        username = "neo4j"
        password = "password"
        self.driver = GraphDatabase.driver(uri, auth=(username, password))

    def request(self, sentence_id, concept, annotation):
        annotation = annotation.strip().lower()

        if " " in annotation:
            wordsIds = self.getIdMultiTokens(annotation, sentence_id)
        else:
            wordsIds = self.getIdSingleTokens(annotation, sentence_id)

        if len(wordsIds) == 0:
            print(f"Error: {annotation} {sentence_id} {wordsIds}")
            return (set(), set())

        refinedOutput = set()
        if concept == "action":
            refinedOutput = self.action(wordsIds)
        elif concept == "actor":
            refinedOutput = self.actor(wordsIds)
        elif concept == "artifact":
            refinedOutput = self.artifact(wordsIds)
        elif concept == "condition":
            refinedOutput = self.condition(wordsIds, sentence_id)
        elif concept == "location":
            refinedOutput = self.location(wordsIds)
        elif concept == "modality":
            refinedOutput = self.modality(wordsIds)
        elif concept == "time":
            refinedOutput = self.time(wordsIds)
        elif concept == "reference":
            refinedOutput = self.reference(wordsIds)

        return (set(wordsIds), refinedOutput)

    def action(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type in ["VN", "VPinf", "VPpart"]
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def actor(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type in ["NP"]
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def artifact(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type in ["NP"]
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def condition(self, wordsIds, sentence_id):
        sentence_id = str(sentence_id) + "."
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL*..]->(w:Word)
            where w.id in $array
            and c.type in ["Srel", "PP"]
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            
            UNION
            
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where w.id in $array
            and c.type in ["Ssub"]
            with c as constituent
            match (constituent)-[:CONSREL]->(w:Word)
            return distinct w.id
            
            UNION
                                
            match (c1:Constituent)-[:CONSREL]->(c2:Constituent)
            WHERE NOT EXISTS {
                MATCH (c2)-[:CONSREL*..]->(w:Word)
                WHERE w.id in $array
            }
            and c1.type = "NP"
            and c2.type in ["VPpart", "VPinf"]
            with c2 as constituent
            match (constituent)-[:CONSREL]->(w:Word)
            where w.id starts with $sentence_id
            return distinct w.id
            ''',
            array=list(wordsIds),
            sentence_id=sentence_id
        )
        return set([record[0] for record in nodes.records])

    def location(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type in ["NP"]
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def modality(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type in ["VN"]
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            
            UNION
                
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type = "SENT"
            and w.id in $array
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def time(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type = "NP"
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            
            UNION
            
            match (c1:Constituent {type: "PP"})-[:CONSREL]->(c2:Constituent {type: "P+"})-[:CONSREL]->(w:Word)
            match (c2)<-[:CONSREL]-(:Constituent)-[:CONSREL]->(:Constituent {type: "NP"})
            where w.id in $array 
            with c1 as c1
            match (c1)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def reference(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            match (c:Constituent)-[:CONSREL]->(w:Word)
            where c.type in ["NP", "PP"]
            and w.id in $array
            with c as constituent
            match (constituent)-[:CONSREL*..]->(w:Word)
            return distinct w.id
            ''',
            array=list(wordsIds)
        )
        return set([record[0] for record in nodes.records])

    def getGoldStandard(self, type="location"):
        nodes = self.driver.execute_query(
            '''
            match (c:Concept)--(w:Word)
            where toInteger(split(w.id, '.')[0]) > 206
            and c.type = $type
            return distinct w.id
            ''',
            type=type
        )

        return set([record[0] for record in nodes.records])

    def getIdMultiTokens(self, annotation, sentence_id):
        annotation = annotation.split(" ")
        listMultiToken = self.driver.execute_query(
            '''
            WITH $array AS words
            MATCH path = (start:Word)-[:NEXT*]->(end:Word)
            where size(words) - 1 = size(relationships(path))
            and start.id starts with $sentence_id
            and all(
                idx IN range(0, size(words)-2)
                WHERE (toLower(words[idx]) = toLower((nodes(path)[idx]).text)
                AND toLower(words[idx+1]) = toLower((nodes(path)[idx + 1]).text))
            )
            and start.text = words[0]
            and end.text = words[size(words) - 1]
            with nodes(path) as result
            unwind result as results
            return collect(results.id) as liste
            ''',
            array=annotation,
            sentence_id=f"{sentence_id}."
        )
        return listMultiToken.records[0][0]

    def getIdSingleTokens(self, annotation, sentence_id):
        listSingleToken = self.driver.execute_query(
            '''
            match (w:Word)
            where toLower(w.text) = $annotation
            and w.id starts with $sentence_id
            with distinct w as results
            return collect(results.id) as liste 
            ''',
            annotation=annotation,
            sentence_id=f"{sentence_id}."
        )
        return listSingleToken.records[0][0]

    def getSpan(self, wordsIds):
        nodes = self.driver.execute_query(
            '''
            MATCH (w:Word)
            where w.id in $array
            with w
            ORDER BY toInteger(split(w.id, '.')[0]) ASC, toInteger(split(w.id, '.')[1]) ASC, toInteger(split(w.id, '.')[2]) ASC
            MATCH (w)
            RETURN REDUCE(s = '', word IN COLLECT(w) | s + ' ' + word.text) AS fullSentence
            ''',
            array=wordsIds
        )
        return " ".join([record[0] for record in nodes.records])