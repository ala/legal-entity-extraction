import pandas as pd

path = ''

df_fs = pd.read_csv('../../results/LLM/GPT-4/GPT-4_few_shot_full_results.csv')
df_zs = pd.read_csv('../../results/LLM/GPT-4/GPT-4_zero_shot_full_results.csv')

df_fs['expert_eval'] = ''

for index, row in df_fs.iterrows():
    if row['status'] != 'out_of_scope':
        continue

    result= df_zs.loc[
            (df_zs['concept'] == row['concept']) &
            (df_zs['llm_annot'] == row['llm_annot']) &
            (df_zs['expert_eval'] == 'x')
        ]

    if not result.empty:
        df_fs.at[index, 'expert_eval'] = 'x'

df_fs.to_csv('../../results/LLM/GPT-4/GPT-4_few_shot_full_results.csv', index=False)
