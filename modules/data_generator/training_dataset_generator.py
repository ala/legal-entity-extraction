import json
import pandas as pd
import importlib

spec = importlib.util.spec_from_file_location("utils", "../llm/utils.py")
utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(utils)

data = pd.read_csv('../../data/dataQS.csv', delimiter=',', low_memory=False, encoding='utf-8')

output = {}

# Converting the original CSV and grouping by sentence
for index, row in data.iterrows():

    if type(row['content']) == float:
        print(f"The row {index} has a float value")
        continue

    if row['sentence'] not in output.keys():
        output[row['sentence']] = {}

    if row['concept'] not in output[row['sentence']]:
        output[row['sentence']][row['concept']] = []

    output[row['sentence']][row['concept']].append(row['content'])

    if row['content'] not in row['sentence']:
        print(f"The row {index + 2} is not a part of the sentence : {row['content']}")

# Create the fine-tuned dataset
filter = ["action", "actor", "artifact", "condition", "location", "modality", "reference", "time"]

input_for_finetuned = []
output_for_finetuned = []

for sentence in output:

    temp_output = {}

    for concept in output[sentence]:
        if concept in filter:
            temp_output[concept] = output[sentence][concept]

    input_for_finetuned.append(f'<s> [INST] {utils.get_pre_prompt_zero_shot()} [/INST]\nUser: {sentence}\nAssistant: ')
    output_for_finetuned.append(f'{json.dumps(temp_output, ensure_ascii=False)} </s>')

finetuned_dataset = {'input': input_for_finetuned, 'output': output_for_finetuned}
df = pd.DataFrame(finetuned_dataset)
df.to_csv('../../data/finetuned_dataset.csv', index=False)