import json
import pandas as pd


data = pd.read_csv('../../data/evalQS.csv', delimiter=',' , low_memory=False, encoding='utf-8')

output = {}

for index, row in data.iterrows():

    if type(row['content']) == float:
        print(f"The row {index} has a float value")
        continue

    if row['sentence'] not in output.keys():
        output[row['sentence']] = {}

    if row['concept'] not in output[row['sentence']]:
        output[row['sentence']][row['concept']] = []

    output[row['sentence']][row['concept']].append(row['content'])

    if row['content'] not in row['sentence']:
        print(f"The row {index+2} is not a part of the sentence : {row['content']}")

with open('../../data/evalQS.json', 'w', encoding='utf-8') as file:
    json.dump(output, file)