import json
import jsonlines
allowed_tokens = ["action", "actor", "artifact", "condition", "location", "modality", "reference", "time"]

def find_all_occurrences(text, phrase):
    start = 0
    while True:
        start = text.find(phrase, start)
        if start == -1: return
        yield start
        start += len(phrase)  # déplacez start après cette occurrence pour trouver la suivante


with open('../../data/dataQS.json', 'r', encoding='utf-8') as f:
    data = json.load(f)
    global_output = []

    for index, (sentence, annot) in enumerate(data.items()):
        sentence = sentence.strip()
        sentence_output = {
            'id': f"train_{index}",
            'text': sentence,
            'tags': []
        }

        for tag, spans in annot.items():

            if tag not in allowed_tokens:
                continue
            
            for span in spans:
                span = span.strip()
                for start_index in find_all_occurrences(sentence, span):
                    end_index = start_index + len(span)
                    sentence_output['tags'].append({
                        "start": start_index if start_index == 0 else start_index-1,
                        "end": end_index,
                        "tag": tag
                    })
                    
        global_output.append(sentence_output)

    with jsonlines.open('../../data/annotations.train.jsonlines', mode='w') as writer:
        for item in global_output:
            writer.write(item)
