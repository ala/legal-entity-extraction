import json
import jsonlines
import copy
allowed_tokens = ["action", "actor", "artifact", "condition", "location", "modality", "reference", "time"]

def find_all_occurrences(text, phrase):
    start = 0
    while True:
        start = text.find(phrase, start)
        if start == -1: return
        yield start
        start += len(phrase)  # déplacez start après cette occurrence pour trouver la suivante

def find_word_before_phrase(sentence, phrase):
    words = sentence.split()
    phrase_words = phrase.split()
    phrase_length = len(phrase_words)

    for i in range(len(words) - phrase_length):
        if words[i:i + phrase_length] == phrase_words:
            return words[i - 1] if i > 0 else None

    return None


def main():
    with open('../../results/Hybrid/GPT4+RB.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    global_output = []

    for index, (sentence, annot) in enumerate(data.items()):
        span = sentence.strip()
        sentence_output = {
            'id': f"eval_{index}",
            'text': sentence,
            'tags': []
        }

        for tag, spans in annot.items():

            if tag not in allowed_tokens:
                continue

            for span in spans:
                span = span.strip()

                words = span.split()
                if words:
                    first_word = words[0].lower()

                    if first_word == "le" or first_word == "les":
                        newSpan = words[1:]
                        newFirstWord = find_word_before_phrase(sentence, " ".join(newSpan))
                        words[0] = newFirstWord if newFirstWord else words[0]


                span = " ".join(words)

                all_occurrences = list(find_all_occurrences(sentence, span))

                if len(all_occurrences) == 0:
                    print(f"=== Erreur all_occurrences null ===\n ➡️{span}\n ➡️{sentence}\n")

                for start_index in all_occurrences:
                    end_index = start_index + len(span)
                    sentence_output['tags'].append({
                        "start": start_index if start_index == 0 else start_index-1,
                        "end": end_index,
                        "tag": tag
                    })

        global_output.append(sentence_output)

    with jsonlines.open('../../data/bert+rb.annotations.train.jsonlines', mode='w') as writer:
        for item in global_output:
            writer.write(item)

if __name__ == '__main__':
    main()