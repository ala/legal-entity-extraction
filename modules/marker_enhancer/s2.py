import pandas as pd
from transformers import AutoTokenizer, AutoModelForMaskedLM
import torch


class MarkerEnhancerLegalRoberta:
    fullMarkers = []
    augmentedMarkers = {}
    sentenceStorage = {}

    def __init__(self, fullMarkers, CSVFile, concept):
        self.fullMarkers = fullMarkers
        self.augmentedMarkers = {element: set() for element in fullMarkers}
        self.sentenceStorage = {element: set() for element in fullMarkers}

        for csv in CSVFile:
            data = pd.read_csv(csv, delimiter=',' , low_memory=False)
            for index, sentence in data[data['concept'] == concept.lower()].iterrows():
                for element in self.sentenceStorage:
                    if element in sentence['content']:
                        self.sentenceStorage[element].add(sentence['sentence'])

    def exec(self):
        tokenizer = AutoTokenizer.from_pretrained("joelito/legal-french-camembert-large")
        model = AutoModelForMaskedLM.from_pretrained("joelito/legal-french-camembert-large")

        for marker, sentences in self.sentenceStorage.items():
            for sentence in sentences:
                index = sentence.find(marker)
                generatedMarkers = self.generateCandidate(
                    text=sentence[:index] + sentence[(index + len(marker)):],
                    maskIndex=index,
                    nbToken=1,
                    nbCandidatePerToken=5,
                    tokenizer=tokenizer,
                    model=model
                )
                if marker in generatedMarkers:
                    generatedMarkers.remove(marker)
                self.augmentedMarkers[marker] = self.augmentedMarkers[marker] | set(generatedMarkers)

        return self

    def generateByRoberta(self, sentence, k, tokenizer, model):
        tokenized_sentence = tokenizer.encode(sentence, add_special_tokens=True, return_tensors="pt")

        mask_token_index = torch.where(tokenized_sentence == tokenizer.mask_token_id)[1].tolist()[0]

        with torch.no_grad():
            predictions = model(tokenized_sentence)[0]
            masked_token_predictions = predictions[0, mask_token_index].topk(k=k).indices

        predicted_tokens = tokenizer.convert_ids_to_tokens(masked_token_predictions.tolist())
        return [token.replace("Ġ", "") for token in predicted_tokens]

    def generateCandidate(self, text, maskIndex, nbToken, nbCandidatePerToken, tokenizer, model):
        complexity = nbCandidatePerToken + nbCandidatePerToken * sum(
            nbCandidatePerToken ** i for i in range(1, nbToken))
        # préparatifs
        generatedTokens = []

        generatedTokens.append(
            self.generateByRoberta(text[:maskIndex] + "<mask>" + text[maskIndex:], nbCandidatePerToken, tokenizer,
                                   model))

        # exploration combinatoire
        for t in range(1, nbToken):
            currentLayer = []
            for el in generatedTokens[t - 1]:
                generated = self.generateByRoberta(text[:maskIndex] + f"{el} <mask>" + text[maskIndex:],
                                                   nbCandidatePerToken, tokenizer, model)
                for el2 in generated:
                    if not el2.endswith(','):
                        currentLayer.append(f"{el} {el2}")

            generatedTokens.append(currentLayer)

        output = []
        for el in generatedTokens:
            for el2 in el:
                if el2 in output:
                    continue
                output.append(el2)
        return output

    def getAugmentedMarkers(self):
        return self.augmentedMarkers

    def getFullMarkers(self):
        return self.fullMarkers

    def getAugmentedMarkersWithoutDoublon(self):
        augmentedMarkersWithoutDoublon = set()
        for key, value in self.augmentedMarkers.items():
            augmentedMarkersWithoutDoublon.update(list(value))
        return augmentedMarkersWithoutDoublon