import pandas as pd
from numpy.core._multiarray_umath import empty


class MarkerEnhancerSemanticRessources:
    fullMarkers = []
    augmentedMarkers = {}

    def __init__(self, fullMarkers):
        self.fullMarkers = fullMarkers
        self.augmentedMarkers = {element: set() for element in fullMarkers}

    def exec(self, lemma_setting=False, top_setting=5):
        if lemma_setting:
            return self.execWithLemma(top_setting)
        else:
            return self.execWithoutLemma(top_setting)

    def execWithoutLemma(self, top_setting):
        scores = {element: {} for element in self.fullMarkers}
        with open("data/20230731-LEXICALNET-JEUXDEMOTS-R5.txt", "r", encoding='ISO-8859-1') as fichier:
            for ligne in fichier:
                if ligne.startswith(";"):
                    ligne = ligne.split(";")
                    if ligne[1] in self.fullMarkers:
                        scores[ligne[1]][ligne[2]] = ligne[3]

        for key, element in scores.items():
            top = sorted(element.items(), key=lambda x: x[1], reverse=True)[:top_setting]
            for new_marker, value in top:
                self.augmentedMarkers[key].add(new_marker)

        return self

    def execWithLemma(self, top_setting):
        scores = {element: {} for element in self.fullMarkers}
        with open("data/20230731-LEXICALNET-JEUXDEMOTS-R5.txt", "r", encoding='ISO-8859-1') as fichier:
            for ligne in fichier:
                if ligne.startswith(";"):
                    ligne = ligne.split(";")
                    if ligne[1] in self.fullMarkers:
                        scores[ligne[1]][ligne[2]] = ligne[3]

        df1 = pd.read_csv('data/lefff-3.4.csv', sep='\t', header=None)
        df2 = pd.read_csv('data/lefff-3.4-addition.csv', sep='\t', header=None)
        df = pd.concat([df1, df2])

        for key, element in scores.items():
            top = [x[0] for x in (sorted(element.items(), key=lambda x: x[1], reverse=True)[:top_setting])]

            mergingList = []
            for new_marker in top:
                result = self.getOtherLemma(new_marker, df)
                if result is not None and result:
                    mergingList = mergingList + result
            top = top + mergingList

            top = top + self.getOtherLemma(key, df)

            for new_marker in top:
                self.augmentedMarkers[key].add(new_marker)

        return self

    def getAugmentedMarkers(self):
        return self.augmentedMarkers

    def getFullMarkers(self):
        return set(self.fullMarkers)

    def getOtherLemma(self, lemma, df):
        resultats = df.loc[df[0] == lemma]
        mySet = set()
        for resultat in resultats.itertuples():
            mySet.add(resultat[3])

        mySet2 = set()
        for el in mySet:
            resultats = df.loc[df[2] == el]
            for resultat in resultats.itertuples():
                mySet2.add(resultat[1])

        if lemma in mySet2:
            mySet2.remove(lemma)

        return list(mySet2)

    def getAugmentedMarkersWithoutDoublon(self):
        augmentedMarkersWithoutDoublon = set()
        for key, value in self.augmentedMarkers.items():
            augmentedMarkersWithoutDoublon.update(list(value))
        return augmentedMarkersWithoutDoublon
