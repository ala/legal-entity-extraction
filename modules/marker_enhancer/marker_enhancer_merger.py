class MarkerEnhancerMerger:
    fullMarkers = []
    augmentedMarkers = {}
    marker1 = None
    marker2 = None

    def __init__(self, marker1, marker2):
        self.marker1 = marker1
        self.marker2 = marker2

    def exec(self):
        self.fullMarkers = list(set(self.marker1.getFullMarkers() + self.marker2.getFullMarkers()))
        self.augmentedMarkers.update(self.marker1.getAugmentedMarkers())
        self.augmentedMarkers.update(self.marker2.getAugmentedMarkers())
        return self

    def getAugmentedMarkers(self):
        return self.augmentedMarkers

    def getFullMarkers(self):
        return self.fullMarkers
