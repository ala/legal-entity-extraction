import importlib
import json
import sys
from alive_progress import alive_bar
import torch
import transformers
import bitsandbytes, flash_attn
import time

# Record the start time
start_time = time.time()

spec = importlib.util.spec_from_file_location("utils", "../utils.py")
utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(utils)

model_id = "../../../models/Mistral-7B-Instruct-v0.2"

model = transformers.AutoModelForCausalLM.from_pretrained(
    model_id,
    #torch_dtype=torch.float16,
    device_map="auto",
    load_in_8bit=False,
    load_in_4bit=True,
    #attn_implementation="flash_attention_2"
)
model.eval()

tokenizer = transformers.AutoTokenizer.from_pretrained(model_id)

generate_text = transformers.pipeline(
    model=model, tokenizer=tokenizer,
    return_full_text=False,  # if using langchain set True
    task="text-generation",
    # we pass model parameters here too
    do_sample=True,
    temperature=0.5,  # 'randomness' of outputs, 0.0 is the min and 1.0 the max
    top_p=0.15,  # select from top tokens whose probability add up to 15%
    top_k=0,  # select from top 0 tokens (because zero, relies on top_p)
    max_new_tokens=2048,  # max number of tokens to generate in the output
    repetition_penalty=1.0  # if output begins repeating increase
)

def instruction_format(sys_message: str, query: str):
    # note, don't "</s>" to the end
    return f'<s> [INST] {sys_message} [/INST]\nUser: {query}\nAssistant: '


with open('../../../data/evalQS.json', 'r', encoding='utf-8') as file:
    loaded = json.load(file)

input = []
output = {}

with alive_bar(len(loaded)) as bar:
    for sentence in loaded:
        input.append(instruction_format(utils.get_pre_prompt_one_shot(), sentence))
        bar()
print("Input creation finished")

res = generate_text(input)

i = 0
for sentence in loaded:
    output[sentence] = res[i][0]["generated_text"]
    i += 1


with open('../../../results/LLM/Mistral-7b/MISTRAL_few_shot_raw_answers.json', 'w', encoding='utf-8') as file:
    json.dump(output, file)  # in 44:36.6 (0.08/s)

print("========== Program finished ==========")

# Record the end time
end_time = time.time()
# Calculate the execution time
execution_time = end_time - start_time

# Calculate hours, minutes, and seconds
hours = execution_time // 3600
minutes = (execution_time % 3600) // 60
seconds = execution_time % 60

# Print the execution time in hours, minutes, and seconds
print(f"The program ran for {int(hours)} hours, {int(minutes)} minutes, and {seconds:.2f} seconds.")