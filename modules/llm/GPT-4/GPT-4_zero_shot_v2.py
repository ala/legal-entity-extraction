import json
from alive_progress import alive_bar
from openai import OpenAI
import importlib.util
import time

# Record the start time
start_time = time.time()

spec = importlib.util.spec_from_file_location("utils", "../utils.py")
utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(utils)

client = OpenAI()

with open('../../../data/evalQS.json', 'r', encoding='utf-8') as file:
    loaded = json.load(file)

output = {}

with alive_bar(len(loaded)) as bar:
    for sentence in loaded:
        try:
            completion = client.chat.completions.create(
                model="gpt-4",
                messages=[
                    {"role": "system", "content": utils.get_pre_prompt_zero_shot_v2()},
                    {"role": "user", "content": sentence}
                ]
            )

            output[sentence] = completion.choices[0].message.content
        except Exception as e:
            print(f"Erreur avec la phrase : {sentence} | {e}")
        bar()

with open('../../../results/LLM/GPT-4/GPT-4_zero_shot_v2_raw_answers.json', 'w', encoding='utf-8') as file:
    json.dump(output, file)

print("========== Program finished ==========")

# Record the end time
end_time = time.time()
# Calculate the execution time
execution_time = end_time - start_time

# Calculate hours, minutes, and seconds
hours = execution_time // 3600
minutes = (execution_time % 3600) // 60
seconds = execution_time % 60

# Print the execution time in hours, minutes, and seconds
print(f"The program ran for {int(hours)} hours, {int(minutes)} minutes, and {seconds:.2f} seconds.")

## The program ran for 0 hours, 18 minutes, and 54.27 seconds.