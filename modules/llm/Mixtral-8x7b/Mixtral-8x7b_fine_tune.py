import torch
import transformers
from datasets import load_dataset
from transformers import AutoTokenizer, AutoModelForCausalLM, Trainer, TrainingArguments
from peft import prepare_model_for_kbit_training, LoraConfig, get_peft_model, PeftModel
import transformers, json, importlib
from alive_progress import alive_bar
import bitsandbytes, flash_attn
import time

# Record the start time
start_time = time.time()

spec = importlib.util.spec_from_file_location("utils", "../utils.py")
utils = importlib.util.module_from_spec(spec)
spec.loader.exec_module(utils)

def fine_tuned(base_model, new_model):
    tokenizer = AutoTokenizer.from_pretrained(base_model)
    model = AutoModelForCausalLM.from_pretrained(base_model,
                                                 load_in_4bit=True,
                                                 torch_dtype=torch.float16,
                                                 device_map="auto",
                                                 )

    # Prepare model for k-bit training
    model = prepare_model_for_kbit_training(model)

    tokenizer.pad_token = "!"

    LORA_R = 8
    LORA_ALPHA = 2 * LORA_R
    LORA_DROPOUT = 0.1

    config = LoraConfig(
        r=LORA_R,
        lora_alpha=LORA_ALPHA,
        target_modules=[
            "q_proj",
            "k_proj",
            "v_proj",
            "o_proj",
            "w1",
            "w2",
            "w3",
            "lm_head",
        ],
        lora_dropout=LORA_DROPOUT,
        bias="none",
        task_type="CAUSAL_LM"
    )

    model = get_peft_model(model, config)

    dataset = load_dataset('csv', data_files='../../../data/finetuned_dataset.csv')

    train_data = dataset["train"] # Not using evaluation data

    def generate_prompt(user_query):
        p =  f"{user_query['input']}{user_query['output']}</s>"
        return p

    def tokenize(prompt):
        return tokenizer(
            prompt + tokenizer.eos_token,
            truncation=True,
            max_length= None ,
            padding="max_length"
        )

    train_data = train_data.shuffle().map(lambda x: tokenize(generate_prompt(x)), remove_columns=["input" , "output"])

    trainer = Trainer(
        model=model,
        train_dataset=train_data,
        args=TrainingArguments(
            per_device_train_batch_size=2,
            gradient_accumulation_steps=2,
            num_train_epochs=6,
            learning_rate=1e-4,
            logging_steps=2,
            optim="adamw_torch",
            save_strategy="steps",
            output_dir="./results"
        ),
        data_collator=transformers.DataCollatorForLanguageModeling(tokenizer, mlm=False)
    )
    model.config.use_cache = False

    trainer.train()

    # Save the fine-tuned model
    trainer.model.save_pretrained(new_model)
    model.eval()

def generate(base_model, new_model):

    base_model_reload = transformers.AutoModelForCausalLM.from_pretrained(
        base_model,
        #torch_dtype=torch.float16,
        device_map="auto",
        load_in_8bit=False,
        load_in_4bit=True,
        #attn_implementation="flash_attention_2"
    )
    model = PeftModel.from_pretrained(base_model_reload, new_model)
    model = model.merge_and_unload()

    tokenizer = transformers.AutoTokenizer.from_pretrained(base_model)

    generate_text = transformers.pipeline(
        model=model, tokenizer=tokenizer,
        return_full_text=False,  # if using langchain set True
        task="text-generation",
        # we pass model parameters here too
        do_sample=True,
        temperature=0.5,  # 'randomness' of outputs, 0.0 is the min and 1.0 the max
        top_p=0.15,  # select from top tokens whose probability add up to 15%
        top_k=0,  # select from top 0 tokens (because zero, relies on top_p)
        max_new_tokens=2048,  # max number of tokens to generate in the output
        repetition_penalty=1.0  # if output begins repeating increase
    )

    def instruction_format(sys_message: str, query: str):
        # note, don't "</s>" to the end
        return f'<s> [INST] {sys_message} [/INST]\nUser: {query}\nAssistant: '


    with open('../../../data/evalQS.json', 'r', encoding='utf-8') as file:
        loaded = json.load(file)

    input = []
    output = {}

    with alive_bar(len(loaded)) as bar:
        for sentence in loaded:
            input.append(instruction_format(utils.get_pre_prompt_zero_shot(), sentence))
            bar()
    print("Input creation finished")

    res = generate_text(input)

    i = 0
    for sentence in loaded:
        output[sentence] = res[i][0]["generated_text"]
        i += 1


    with open('../../../results/LLM/Mixtral-8x7b/MIXTRAL_fine_tuned_raw_answers.json', 'w', encoding='utf-8') as file:
        json.dump(output, file)  # in 44:36.6 (0.08/s)


#######################################################################################################################

base_model = "../../../models/Mixtral-8x7B-Instruct-v0.1"
new_model = "../../../models/Fine-tuned_Mixtral-8x7b"

fine_tuned(base_model, new_model)
generate(base_model, new_model)

print("========== Program finished ==========")

# Record the end time
end_time = time.time()
# Calculate the execution time
execution_time = end_time - start_time

# Calculate hours, minutes, and seconds
hours = execution_time // 3600
minutes = (execution_time % 3600) // 60
seconds = execution_time % 60

# Print the execution time in hours, minutes, and seconds
print(f"The program ran for {int(hours)} hours, {int(minutes)} minutes, and {seconds:.2f} seconds.")