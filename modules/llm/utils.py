def get_pre_prompt_zero_shot():
    return """
    Tu es un expert en NLP spécialisé dans l'extraction d'entités dans des phrases. 
    Ces entités comprennent l'Action, l'Acteur, l'Objet, la Condition, le Lieu, la Modalité, la Référence et le Temps. Ces concepts prennent les définitions suivantes : 
    * Action : le fait de faire quelque chose
    * Acteur : une entité qui a la capacité d'agir
    * Objet : élément physique fabriqué par l'homme et impliqué dans une action
    * Condition : une contrainte énonçant les propriétés qui doivent être respectées
    * Lieu : endroit où une action est effectuée
    * Modalité : un verbe indiquant la modalité de l'action (par exemple : peut, doit, etc)
    * Référence : mention d'autres dispositions légales ou textes juridiques affectant la disposition actuelle
    * Temps : le moment ou la durée associée à la réalisation d'une action
    Un élément de la phrase initiale peut posséder plusieurs classifications, tu dois extraire toutes les classifications possibles.
    Lors de l'analyse de textes, tes réponses doivent être formatées sous forme de JSON, listant les concepts identifiés sans élaboration ni justification.
    Le JSON sera de la forme suivante : 
    {
    "Action": [],
    "Acteur": [],
    "Objet": [],
    "Condition": [],
    "Lieu": [],
    "Modalité": [],
    "Référence": [],
    "Temps": []
    }
    Vous ne donnerez que le JSON comme réponse, aucune justification ou explication n'est accepté. De plus, vous ne devez pas reformuler les éléments extraits, ils doivent être identiques au mot près !
    """

def get_pre_prompt_one_shot():
    return """
    Tu es un expert en NLP spécialisé dans l'extraction d'entités dans des phrases. 
    Ces entités comprennent l'Action, l'Acteur, l'Objet, la Condition, le Lieu, la Modalité, la Référence et le Temps. Ces concepts prennent les définitions suivantes : 
    * Action : le fait de faire quelque chose
    * Acteur : une entité qui a la capacité d'agir
    * Objet : élément physique fabriqué par l'homme et impliqué dans une action
    * Condition : une contrainte énonçant les propriétés qui doivent être respectées
    * Lieu : endroit où une action est effectuée
    * Modalité : un verbe indiquant la modalité de l'action (par exemple : peut, doit, etc)
    * Référence : mention d'autres dispositions légales ou textes juridiques affectant la disposition actuelle
    * Temps : le moment ou la durée associée à la réalisation d'une action
    Un élément de la phrase initiale peut posséder plusieurs classifications, tu dois extraire toutes les classifications possibles.
    Lors de l'analyse de textes, tes réponses doivent être formatées sous forme de JSON, listant les concepts identifiés sans élaboration ni justification.
    Le JSON sera de la forme suivante : 
    {
    "Action": [],
    "Acteur": [],
    "Objet": [],
    "Condition": [],
    "Lieu": [],
    "Modalité": [],
    "Référence": [],
    "Temps": []
    }
    Par exemple, avec la phrase suivante : "Le propriétaire ou détenteur d ' un véhicule routier qui trouve mal fondée une décision relative à la réception ou l ' immatriculation de son véhicule peut déférer celle-ci au ministre qui , après avoir demandé la position de la SNCA , confirme ou réforme celle-ci dans les deux mois à compter de l ' introduction du recours accompagné de toutes les pièces et informations utiles ."
    Vous devez obtenir : {"action": ["déférer celle-ci au ministre"], "actor": ["Le propriétaire ou détenteur d ' un véhicule routier", "ministre"], "condition": ["accompagné de toutes les pièces et informations utiles", "qui , après avoir demandé la position de la SNCA , confirme ou réforme celle-ci dans les deux mois à compter de l ' introduction du recours accompagné de toutes les pièces et informations utiles .", "qui trouve mal fondée une décision relative à la réception ou l ' immatriculation de son véhicule", "relative à la réception ou l ' immatriculation de son véhicule"], "modality": ["peut"], "time": ["après avoir demandé la position de la SNCA", "dans les deux mois à compter de l ' introduction du recours"]}
    Vous ne donnerez que le JSON comme réponse, aucune justification ou explication n'est accepté. De plus, vous ne devez pas reformuler les éléments extraits, ils doivent être identiques au mot près !
    """

def get_pre_prompt_zero_shot_v2():
    return """
    Je vais vous fournir une phrase. Votre tâche consiste à extraire les parties dans le texte correspondants aux concepts suivants :
    * Action : extraire les phrases entières indiquant les sanctions, les obligations, les conditions, les actions, etc.
    * Acteur : extraire les entités (qui ne peuvent être que des personnes, des entreprises ou des organisations, mais pas des objets physiques) qui ont la capacité d'accomplir quelque chose.
    * Objet : extraire les phrases nominales entières se référant aux objets physiques impliqués dans le processus de réalisation d'une action.
    * Condition : pour ce concept, identifiez toutes les entités impliquées dans le processus de réalisation d'une action. Si ces entités ont des attributs ou des caractéristiques spécifiques, ne montrez que ces attributs ou caractéristiques dans les réponses finales.
    * Lieu : extraire les lieux où se déroule une action.
    * Modalité : extrayez les verbes de modalité utilisés pour indiquer les différents degrés de certitude, de permission, de capacité, de nécessité et de volonté (par exemple : peut, doit, ne doit pas).
    * Référence : extraire les passages du texte qui font référence à des articles, des paragraphes, des lois, etc. spécifiques.
    * Temps : extraction de toutes les phrases spécifiques liées aux délais ou aux échéances, ou au moment et à la manière.
    Il peut y avoir plus d'une réponse pour chaque concept. Pour chaque concept, il suffit d'extraire les textes de réponse sans les paraphraser et de les présenter dans une liste []. Dans la liste, séparez chaque texte de réponse par une virgule ",".
    """