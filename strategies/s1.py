from data.MarkersList import MarkersList
from modules.evaluators.marker_evaluator import MarkerEvaluator
from modules.evaluators.rule_based_extraction_evaluator import RuleBasedExtractionEvaluator
from modules.marker_enhancer.s1 import MarkerEnhancerSemanticRessources
from modules.neo4j.PatternsRequest import PatternsRequest

path = "Reason"
fullMarkers = MarkersList().getReasonMarkers()
sampling_trigger = 50000

# ==================================
#           Markers
# ==================================
mkhsr = MarkerEnhancerSemanticRessources(fullMarkers).exec(lemma_setting=True)
print("mkhsr terminé")

# ==================================
#           Evaluators
# ==================================
print("début de l'évaluation via ressources sémantiques")
MarkerEvaluator.boxPlot(f"./results/S1/MarkerEnhancer/{path}/ViaSemanticRessources", fullMarkers, mkhsr.getAugmentedMarkers(), sampling_trigger)
MarkerEvaluator.heathMap(f"./results/S1/MarkerEnhancer/{path}", fullMarkers, mkhsr.getAugmentedMarkers())
print("fin de l'évaluation via ressources sémantiques")

# ==================================
#           Requests Neo4j
# ==================================
print("début de l'évaluation via Neo4J")
E_augmented = PatternsRequest().reason(mkhsr.getAugmentedMarkers())
E_goldStandard = PatternsRequest().getGoldStandard(path.lower())
print("lancement de l'évaluation via Neo4J")
RuleBasedExtractionEvaluator().evaluate(E_augmented = E_augmented, E_goldStandard= E_goldStandard, fileName=f"./results/S1/MarkerEnhancer/{path}/Result", sampling_trigger=sampling_trigger)
print("fin de l'évaluation via Neo4J")