from data.MarkersList import MarkersList
from modules.evaluators.marker_evaluator import MarkerEvaluator
from modules.evaluators.rule_based_extraction_evaluator import RuleBasedExtractionEvaluator
from modules.marker_enhancer.s2 import MarkerEnhancerLegalRoberta
from modules.marker_enhancer.s1 import MarkerEnhancerSemanticRessources
from modules.neo4j.PatternsRequest import PatternsRequest

path = "Location"
fullMarkers = MarkersList().getLocationMarkers()
sampling_trigger = 50000

# ==================================
#           Markers
# ==================================
mkhlr = MarkerEnhancerLegalRoberta(fullMarkers, ['./data/evalQS.csv'], path).exec()
print("mkhlr terminé")

# ==================================
#           Evaluators
# ==================================
print("début de l'évaluation via modèle BERT")
MarkerEvaluator.boxPlot(f"./results/S2/MarkerEnhancer/{path}/ViaLegalRoberta", fullMarkers, mkhlr.getAugmentedMarkers(), sampling_trigger)
MarkerEvaluator.heathMap(f"./results/S2/MarkerEnhancer/{path}", fullMarkers, mkhlr.getAugmentedMarkers())
print("fin de l'évaluation via modèle BERT")

# ==================================
#           Requests Neo4j
# ==================================
print("début de l'évaluation via Neo4J")
E_augmented = PatternsRequest().location(mkhlr.getAugmentedMarkers())
E_goldStandard = PatternsRequest().getGoldStandard("location")
RuleBasedExtractionEvaluator().evaluate(E_augmented = E_augmented, E_goldStandard= E_goldStandard, fileName=f"./results/S2/MarkerEnhancer/{path}/Result")
print("fin de l'évaluation via Neo4J")
